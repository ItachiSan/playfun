## Makefile made by tom7.
## Heavily reworked by Giovanni 'ItachiSan' Santini <giovannisantini93@yahoo.it>
## Targets.
default: playfun learnfun scopefun 
all: learnfun showfun tasbot playfun scopefun pinviz emu_test objective_test sigbovik weighted-objectives_test


# Compilers section
# Specify the Protobuf compiler name.
PROTOC=protoc


## Build flags section
# Optimization area.
# Profile optimization.
#PROFILE=-pg
PROFILE=
# Link time optimization.
#FLTO=-flto
FLTO=
# General optimization.
#OPT=-O3 -mtune=native -march=native
OPT=-O2

# Linker area.
# Fix SDL link error thanks to http://stackoverflow.com/a/18115484
# Link as much as possible statically on Windows thanks to http://stackoverflow.com/a/14033674 and http://stackoverflow.com/a/28001261
ifneq ($(findstring Windows,$(OS)),)
	SPECIFIC_LIBS=-Wl,-subsystem,console
	SPECIFIC_LIBS+=-static-libgcc -static-libstdc++
	SPECIFIC_LIBS+=-Wl,-Bstatic -lstdc++ -lpthread -lz -lprotobuf -lpng
	SPECIFIC_LIBS+=-Wl,-Bdynamic -lws2_32 -lmingw32 -lSDLmain
else
	SPECIFIC_LIBS=-lz -lprotobuf -lpng
endif
LDLIBS+=$(SPECIFIC_LIBS) -lSDL -lSDL_net
LDFLAGS+=$(OPT) $(FLTO) $(PROFILE)

# Include path area.
INCLUDES=-I "cc-lib" -I "cc-lib/city" -I includes
# Specific MinGW includes directories
ifneq ($(findstring Windows,$(OS)),)
	INCLUDES+=-I /mingw64/include/SDL/
else
	INCLUDES+=-I /usr/include/SDL/
endif

# A special build flag.
# For C preprocessor, allows to use the Marionet system.
CCNETWORKING=-DMARIONET=1

# Build flags.
# C compiler flags.
CXXFLAGS+=-Wall -Wno-deprecated -Wno-sign-compare -Wno-write-strings $(INCLUDES) $(OPT) $(FLTO) $(PROFILE)
# Preprocessor flags.
CPPFLAGS+=$(CCNETWORKING) -DPSS_STYLE=1 -DDUMMY_UI -DHAVE_ASPRINTF -DHAVE_ALLOCA -DNOWINSTUFF -DNO_STDIO_REDIRECT
# C++ compiler flags.
CXXFLAGS+=--std=c++11


## Objects section
# Tom7 stuff.
# Protobuf stuff.
PROTO_HEADERS=marionet.pb.h
PROTO_OBJECTS=marionet.pb.o
# Tom7 cc-lib needed stuff.
CCLIBOBJECTS=cc-lib/util.o cc-lib/arcfour.o cc-lib/base/stringprintf.o cc-lib/city/city.o cc-lib/textsvg.o cc-lib/stb_image.o
# SDL/libpng related stuff.
SDLUTILOBJECTS=sdlutil-lite.o
PNGSAVE_OBJECTS=pngsave.o
# Network related code
NETWORKINGOBJECTS= $(PROTO_OBJECTS) netutil.o
# Code for learnfun/playfun and else
TASBOT_OBJECTS=headless-driver.o simplefm2.o emulator.o basis-util.o objective.o weighted-objectives.o motifs.o util.o

#FCEU related stuff.
MAPPEROBJECTS=fceu/mappers/24and26.o fceu/mappers/51.o fceu/mappers/69.o fceu/mappers/77.o fceu/mappers/40.o fceu/mappers/6.o fceu/mappers/71.o fceu/mappers/79.o fceu/mappers/41.o fceu/mappers/61.o fceu/mappers/72.o fceu/mappers/80.o fceu/mappers/42.o fceu/mappers/62.o fceu/mappers/73.o fceu/mappers/85.o fceu/mappers/46.o fceu/mappers/65.o fceu/mappers/75.o fceu/mappers/emu2413.o fceu/mappers/50.o fceu/mappers/67.o fceu/mappers/76.o fceu/mappers/mmc2and4.o
UTILSOBJECTS=fceu/utils/ConvertUTF.o fceu/utils/general.o fceu/utils/memory.o fceu/utils/crc32.o fceu/utils/guid.o fceu/utils/endian.o fceu/utils/md5.o fceu/utils/xstring.o fceu/utils/unzip.o
BOARDSOBJECTS=fceu/boards/01-222.o fceu/boards/32.o fceu/boards/gs-2013.o fceu/boards/103.o fceu/boards/33.o fceu/boards/h2288.o fceu/boards/106.o fceu/boards/34.o fceu/boards/karaoke.o fceu/boards/108.o fceu/boards/3d-block.o fceu/boards/kof97.o fceu/boards/112.o fceu/boards/411120-c.o fceu/boards/konami-qtai.o fceu/boards/116.o fceu/boards/43.o fceu/boards/ks7012.o fceu/boards/117.o fceu/boards/57.o fceu/boards/ks7013.o fceu/boards/120.o fceu/boards/603-5052.o fceu/boards/ks7017.o fceu/boards/121.o fceu/boards/68.o fceu/boards/ks7030.o fceu/boards/12in1.o fceu/boards/8157.o fceu/boards/ks7031.o fceu/boards/15.o fceu/boards/82.o fceu/boards/ks7032.o fceu/boards/151.o fceu/boards/8237.o fceu/boards/ks7037.o fceu/boards/156.o fceu/boards/830118C.o fceu/boards/ks7057.o fceu/boards/164.o fceu/boards/88.o fceu/boards/le05.o fceu/boards/168.o fceu/boards/90.o fceu/boards/lh32.o fceu/boards/17.o fceu/boards/91.o fceu/boards/lh53.o fceu/boards/170.o fceu/boards/95.o fceu/boards/malee.o fceu/boards/175.o fceu/boards/96.o fceu/boards/mmc1.o fceu/boards/176.o fceu/boards/99.o fceu/boards/mmc3.o fceu/boards/177.o fceu/boards/__dummy_mapper.o fceu/boards/mmc5.o fceu/boards/178.o fceu/boards/a9711.o fceu/boards/n-c22m.o fceu/boards/179.o fceu/boards/a9746.o fceu/boards/n106.o fceu/boards/18.o fceu/boards/ac-08.o fceu/boards/n625092.o fceu/boards/183.o fceu/boards/addrlatch.o fceu/boards/novel.o fceu/boards/185.o fceu/boards/ax5705.o fceu/boards/onebus.o fceu/boards/186.o fceu/boards/bandai.o fceu/boards/pec-586.o fceu/boards/187.o fceu/boards/bb.o fceu/boards/sa-9602b.o fceu/boards/189.o fceu/boards/bmc13in1jy110.o fceu/boards/sachen.o fceu/boards/193.o fceu/boards/bmc42in1r.o fceu/boards/sc-127.o fceu/boards/199.o fceu/boards/bmc64in1nr.o fceu/boards/sheroes.o fceu/boards/208.o fceu/boards/bmc70in1.o fceu/boards/sl1632.o fceu/boards/222.o fceu/boards/bonza.o fceu/boards/smb2j.o fceu/boards/225.o fceu/boards/bs-5.o fceu/boards/subor.o fceu/boards/228.o fceu/boards/cityfighter.o fceu/boards/super24.o fceu/boards/230.o fceu/boards/dance2000.o fceu/boards/supervision.o fceu/boards/232.o fceu/boards/datalatch.o fceu/boards/t-227-1.o fceu/boards/234.o fceu/boards/deirom.o fceu/boards/t-262.o fceu/boards/235.o fceu/boards/dream.o fceu/boards/tengen.o fceu/boards/244.o fceu/boards/edu2000.o fceu/boards/tf-1201.o fceu/boards/246.o fceu/boards/famicombox.o fceu/boards/transformer.o fceu/boards/252.o fceu/boards/fk23c.o fceu/boards/vrc2and4.o fceu/boards/253.o fceu/boards/ghostbusters63in1.o fceu/boards/vrc7.o fceu/boards/28.o fceu/boards/gs-2004.o fceu/boards/yoko.o
INPUTOBJECTS=fceu/input/arkanoid.o fceu/input/ftrainer.o fceu/input/oekakids.o fceu/input/suborkb.o fceu/input/bworld.o fceu/input/hypershot.o fceu/input/powerpad.o fceu/input/toprider.o fceu/input/cursor.o fceu/input/mahjong.o fceu/input/quiz.o fceu/input/zapper.o fceu/input/fkb.o fceu/input/mouse.o fceu/input/shadow.o
FCEUOBJECTS=fceu/asm.o fceu/cart.o fceu/cheat.o fceu/conddebug.o fceu/config.o fceu/debug.o fceu/drawing.o fceu/emufile.o fceu/fceu.o fceu/fds.o fceu/file.o fceu/filter.o fceu/ines.o fceu/input.o fceu/movie.o fceu/netplay.o fceu/nsf.o fceu/oldmovie.o fceu/palette.o fceu/ppu.o fceu/sound.o fceu/state.o fceu/unif.o fceu/video.o fceu/vsuni.o fceu/wave.o fceu/x6502.o
DRIVERS_COMMON_OBJECTS=fceu/drivers/common/args.o fceu/drivers/common/nes_ntsc.o fceu/drivers/common/cheat.o fceu/drivers/common/scale2x.o  fceu/drivers/common/scale3x.o fceu/drivers/common/scalebit.o fceu/drivers/common/hq2x.o fceu/drivers/common/vidblit.o fceu/drivers/common/hq3x.o
# All the needed stuff.
EMUOBJECTS=$(FCEUOBJECTS) $(MAPPEROBJECTS) $(UTILSOBJECTS) $(PALLETESOBJECTS) $(BOARDSOBJECTS) $(INPUTOBJECTS) $(DRIVERS_COMMON_OBJECTS)

#included in all tests, etc.
BASEOBJECTS=$(CCLIBOBJECTS) $(NETWORKINGOBJECTS)
OBJECTS=$(BASEOBJECTS) $(EMUOBJECTS) $(TASBOT_OBJECTS)


## Recipes section
# Protobuf files compilation rules
%.pb.cc: %.proto
	$(PROTOC) $< --cpp_out=.
%.pb.h: %.proto
	$(PROTOC) $< --cpp_out=.
%.pb.o: %.pb.cc

# Real targets
learnfun: $(OBJECTS)
showfun: $(OBJECTS)
tasbot: $(OBJECTS)
playfun: $(OBJECTS)
scopefun: $(OBJECTS) $(PNGSAVE_OBJECTS) wave.o
pinviz: $(OBJECTS) $(PNGSAVE_OBJECTS) wave.o
emu_test: $(OBJECTS)
objective_test: $(BASEOBJECTS) objective.o
sigbovik: $(OBJECTS) $(SDLUTILOBJECTS)
weighted-objectives_test : $(BASEOBJECTS) weighted-objectives.o util.o

# Testing stuff
test : emu_test objective_test weighted-objectives_test
	time ./emu_test
	time ./objective_test
	time ./weighted-objectives_test

# Clean up
clean :
	$(RM) learnfun playfun showfun scopefun tasbot pinviz *_test
	$(RM) *.o $(OBJECTS) $(CCLIBOBJECTS) gmon.out
	$(RM) *.pb.o *.pb.h *.pb.cc

cleantas :
	$(RM) prog*.fm2 deepest.fm2 heuristicest.fm2

veryclean : clean cleantas